# FastMenu ——— 快速菜单

-   **下载** 

[适用于1.16.5以上的版本](https://gitee.com/YYDSQAQ1024/fmenu-depend/releases/download/plugin/fmenu.jar)

[适用于1.16.5以下的版本](https://gitee.com/YYDSQAQ1024/fmenu-depend/releases/download/plugin/fmenu-1.12.2.jar)

- 安装
1. 将插件放入`plugins`文件夹
2. 启动服务器，在控制台输入`fm cloud reset`并回车执行
3. 等待初始化完毕，注意：初始化期间正常不会出现红色的文字！如有，请加Q群询问。

- 联系
1. Q群：651806188
2. [MCBBS](https://www.mcbbs.net/thread-1475165-1-1.html)

- 常见问题

Q：怎么取消菜单插件上限？

A：把`setting.yml`中的`PlayerMaxCreate`设为`99999`


Q：怎么关闭插件权限？

A：把`setting.yml`中`permission`下的所有`allow`设为`true`
